<?php

namespace Learning2020\OAuth2\ClassLink\Client\User;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

class ClassLinkUser implements ResourceOwnerInterface
{
    /**
     * @var array
     */
    protected $response;

    /**
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        $altId = array_key_exists('data', $this->response) && array_key_exists('id', $this->response['data'])
            ? $this->response['data']['id'] : null;

        return $this->response['UserId'] ?? $altId;
    }

    /**
     * Get user data as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->response;
    }
}