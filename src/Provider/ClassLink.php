<?php

namespace Learning2020\OAuth2\ClassLink\Client\Provider;

use Psr\Http\Message\ResponseInterface;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;

use Learning2020\OAuth2\ClassLink\Client\User\ClassLinkUser;
use Learning2020\OAuth2\ClassLink\Client\Exception\ClassLinkIdentityProviderException;

class ClassLink extends AbstractProvider
{
    use BearerAuthorizationTrait;

    /**
     * @link https://developer.classlink.com/docs/obtaining-bearer-tokens
     */
    private const ACCESS_TOKEN_EXPIRES_IN = 24 * 3600; // 24h

    /**
     * @return string
     */
    public function getBaseAuthorizationUrl(): string
    {
        return 'https://launchpad.classlink.com/oauth2/v2/auth';
    }

    /**
     * @param array $params
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params): string
    {
        return 'https://launchpad.classlink.com/oauth2/v2/token';
    }

    /**
     * @param AccessToken $token
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token): string
    {
        return 'https://nodeapi.classlink.com/v2/my/info';
    }

    /**
     * @param array $options
     * @return array|string[]
     */
    protected function getAuthorizationParameters(array $options): array
    {
        return array_merge(
            parent::getAuthorizationParameters($options),
            [
                'response_type' => 'code',
            ]
        );
    }

    /**
     * @return string[]
     */
    protected function getDefaultScopes(): array
    {
        return [
            'profile',
        ];
    }

    /**
     * @param ResponseInterface $response
     * @param array|string $data
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        if ($response->getStatusCode() >= 400) {
            throw ClassLinkIdentityProviderException::clientException($response, $data);
        } else if (isset($data['error'])) {
            throw ClassLinkIdentityProviderException::oauthException($response, $data);
        }
    }

    /**
     * @param array $response
     * @param AccessToken $token
     * @return ClassLinkUser|ResourceOwnerInterface
     */
    protected function createResourceOwner(array $response, AccessToken $token): ResourceOwnerInterface
    {
        return new ClassLinkUser($response);
    }

    /**
     * @param array $result
     * @return array
     */
    protected function prepareAccessTokenResponse(array $result): array
    {
        $result = parent::prepareAccessTokenResponse($result);

        $result['expires_in'] = self::ACCESS_TOKEN_EXPIRES_IN;
        return $result;
    }
}
