<?php

namespace Learning2020\OAuth2\ClassLink\Client\Test\Provider;

use Learning2020\OAuth2\ClassLink\Client\Provider\ClassLink as ClassLinkProvider;

use Mockery as m;

class ClassLinkTest extends \PHPUnit_Framework_TestCase
{
    protected $provider;

    protected function setUp()
    {
        $this->provider = new ClassLinkProvider([
            'clientId' => 'mock_client_id',
            'clientSecret' => 'mock_secret',
            'redirectUri' => 'none',
        ]);
    }

    public function tearDown()
    {
        m::close();
        parent::tearDown();
    }

    public function testAuthorizationUrl()
    {
        $url = $this->provider->getAuthorizationUrl();
        $uri = parse_url($url);
        parse_str($uri['query'], $query);

        $this->assertArrayHasKey('state', $query);
        $this->assertArrayHasKey('scope', $query);
        $this->assertArrayHasKey('response_type', $query);
        $this->assertArrayHasKey('approval_prompt', $query);
        $this->assertArrayHasKey('redirect_uri', $query);
        $this->assertArrayHasKey('client_id', $query);

        $this->assertContains('profile', $query['scope']);

        $this->assertAttributeNotEmpty('state', $this->provider);
    }

    public function testBaseAccessTokenUrl()
    {
        $url = $this->provider->getBaseAccessTokenUrl([]);
        $uri = parse_url($url);
        
        $this->assertEquals('/oauth2/v2/token', $uri['path']);
    }

    public function testResourceOwnerDetailsUrl()
    {
        $token = m::mock('League\OAuth2\Client\Token\AccessToken', [['access_token' => 'mock_access_token']]);
    
        $url = $this->provider->getResourceOwnerDetailsUrl($token);
        $uri = parse_url($url);
    
        $this->assertEquals('/v2/my/info', $uri['path']);
        $this->assertNotContains('mock_access_token', $url);
    }

    public function testUserData()
    {
        $response = json_decode('{"UserId": 3549981,"TenantId": 2,"StateId": 2,"StateName": "New Jersey","BuildingId": 1,"AuthenticationType": 2,"DisplayName": "ClassLink S. Dev 31","FirstName": "ClassLink","LastName": "Dev 31","Email": "cldev31-s@cldemo.local.local","LoginId": "cldev31-s","ImagePath": "","LanguageId": 1,"Language": "en","DefaultTimeFormat": 0,"Profile": "Developer Student Profile","ProfileId": 3280,"Tenant": "ClassLink Demo","Building": "CL Demo Building1","Role": "Student","Role_Level": 4,"LastAccessTime": "2021-11-11T11:11:26.109Z","OrgId": "","SourcedId": ""}', true);

        $provider = m::mock('Learning2020\OAuth2\ClassLink\Client\Provider\ClassLink[fetchResourceOwnerDetails]')
            ->shouldAllowMockingProtectedMethods();
    
        $provider->shouldReceive('fetchResourceOwnerDetails')
            ->times(1)
            ->andReturn($response);
    
        $token = m::mock('League\OAuth2\Client\Token\AccessToken');
        $user = $provider->getResourceOwner($token);
        
        $this->assertInstanceOf('Learning2020\OAuth2\ClassLink\Client\User\ClassLinkUser', $user);
    
        $this->assertEquals(3549981, $user->getId());

        $user = $user->toArray();

        $this->assertArrayHasKey('UserId', $user);
    }
    
    public function testUserDataUnknownUserType()
    {
        $response = json_decode('{"UserId": 3549981,"TenantId": 2,"StateId": 2,"StateName": "New Jersey","BuildingId": 1,"AuthenticationType": 2,"DisplayName": "ClassLink S. Dev 31","FirstName": "ClassLink","LastName": "Dev 31","Email": "cldev31-s@cldemo.local.local","LoginId": "cldev31-s","ImagePath": "","LanguageId": 1,"Language": "en","DefaultTimeFormat": 0,"Profile": "Developer Student Profile","ProfileId": 3280,"Tenant": "ClassLink Demo","Building": "CL Demo Building1","Role": "District Admin","Role_Level": 4,"LastAccessTime": "2021-11-11T11:11:26.109Z","OrgId": "","SourcedId": ""}', true);

        $provider = m::mock('Learning2020\OAuth2\ClassLink\Client\Provider\ClassLink[fetchResourceOwnerDetails]')
            ->shouldAllowMockingProtectedMethods();
    
        $provider->shouldReceive('fetchResourceOwnerDetails')
            ->times(1)
            ->andReturn($response);
    
        $token = m::mock('League\OAuth2\Client\Token\AccessToken');
        $user = $provider->getResourceOwner($token);
        
        $this->assertInstanceOf('League\OAuth2\Client\Provider\ResourceOwnerInterface', $user);
        $this->assertInstanceOf('Learning2020\OAuth2\ClassLink\Client\User\ClassLinkUser', $user);
    }
}